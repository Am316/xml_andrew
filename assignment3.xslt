<?xml version="1.0" encoding="UTF-8"?>
<!-- Andrew Murphy-->
<!-- December 01 -->
	<!-- Nice work here Andrew (et all).  Your XSL properly outputs and formats the xml data
10/10 
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/" name="xsl:initial-template">
	<html>
		<head>
			<title>Telephone Bill</title> 
		</head>
		<body>
	     <div><strong>Customer Info</strong></div>

		
							name:<xsl:value-of select="telephoneBill/customer/name"/><br/>
							address:<xsl:value-of select="telephoneBill/customer/address"/><br/>
							city:<xsl:value-of select="telephoneBill/customer/city"/><br/>
							province:<xsl:value-of select="telephoneBill/customer/province"/>
				
		<table border="1">
			<tbody>
				<tr>
					<th>Called number</th>
					<th>Date</th>
					<th>Duration In Minutes</th>
					<th>charge</th>
				</tr>
			<xsl:for-each select="telephoneBill/call">
				<tr>
					<td><xsl:value-of select="@number"/></td>
					<td><xsl:value-of select="@date"/></td>
					<td><xsl:value-of select="@durationInMinutes"/></td>
					<td><xsl:value-of select="@charge"/></td>
				</tr>
			</xsl:for-each>
			
			</tbody>
		</table>
		</body>
	</html>
	
	</xsl:template>
</xsl:stylesheet>
